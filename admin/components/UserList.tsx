import React, { useState, useEffect, useRef } from 'react';
import UserListItem from './UserListItem';
import '../styles/form.css'
import Modal from './Modal';
import UserRights from './UserRights';
import Table, { Header, HeaderSize } from './Table/Table';
import NewUser from './NewUser';
import { User, UserForm } from '../types/User';

const headers: Header[] = [
  { name: 'Id', size: HeaderSize.SMALL },
  { name: 'Name' },
  { name: 'Email', size: HeaderSize.LARGE },
  { name: 'Password' },
  { name: 'Actions' }
]

export default function UserList() {
  const [users, setUsers] = useState<User[]>([])
  const [showAccessRights, setShowAccessRights] = useState<number | boolean>(false);

  useEffect(() => {
    reloadUsers()
  }, [])

  async function reloadUsers() {
    const response = await fetch('http://localhost:4000/v1/users')
    const users: User[] = await response.json()
    setUsers(users.sort((userA, userB) => userA.id - userB.id))
  }

  async function createUser(userForm: UserForm) {
    const mockUser: User = {
      id: users.length > 0 ? users[users.length - 1].id + 1 : 0,
      ...userForm,
      created: true
    }
    setUsers([mockUser, ...users])
    const response = await fetch('http://localhost:4000/v1/users', {
      method: 'POST',
      body: JSON.stringify(userForm),
      headers: {
        'Content-Type': 'application/json'
      },
    })
    const newUser: User = await response.json()
    reloadUsers()
  }

  async function deleteUser(id: number) {
    setUsers(users => users.map(user => {
      if (user.id === id) {
        user.deleted = true
      }
      return user
    }))
    const response = await fetch(`http://localhost:4000/v1/users/${id}`, { method: 'DELETE' })
    setUsers(users => users.filter(user => user.id !== id))
    setTimeout(() => {
      reloadUsers()
    }, 1000)
  }

  return (
    <div className="userlist">
      <Table headers={headers} >
        {users.map(user => <UserListItem key={user.id} user={user} onClickDelete={deleteUser} onClickShowRights={setShowAccessRights} />)}
      </Table>
      <div className="userlist__footer">
        <NewUser onSubmit={createUser} />
      </div>
      <Modal id="accessrights" isOpen={showAccessRights !== false} onClose={() => setShowAccessRights(false)} showClose={true} showSubmit={true} >
        <UserRights id={showAccessRights} />
      </Modal>
    </div>
  )
}