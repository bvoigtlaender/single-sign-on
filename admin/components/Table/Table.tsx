import React from 'react'
import './table.css'

export enum HeaderSize {
  SMALL = 'small',
  DEFAULT = 'default',
  LARGE = 'large'
}

export type Header = {
  name: string,
  large?: boolean,
  size?: HeaderSize
}

type Props = {
  headers: Header[],
  children: React.ReactNode
}

export default function Table({ headers, children }: Props) {
  return (
    <div className="table">
      <div className='table__row table__row--header'>
        {headers.map((header, index) => <p className={`table__column table__column--${header.size}`} key={index}>{header.name}</p>)}
      </div>
      {children}
    </div>
  )
}