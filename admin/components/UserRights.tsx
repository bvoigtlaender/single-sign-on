import React, { useEffect, useState } from 'react';
import { UserDetailed } from '../types/User';

type Props = {
  id: number | boolean
}

export default function UserRights({ id }: Props) {
  const [user, setUser] = useState<UserDetailed>()
  useEffect(() => {
    if (typeof id === 'number') {
      fetch(`http://localhost:4000/v1/users/${id}`)
        .then(response => response.json())
        .then(setUser)
    }
  }, [id])

  if (user) return <div>
    <h1>Access rights for {user.name}:</h1>
    <table>
      <tbody>
        {user.accessRights.map(accessRight =>
          <tr>
            {accessRight}
          </tr>
        )}
      </tbody>
    </table>
  </div>

  return <div>
    <h1>Loading...</h1>
  </div>
}