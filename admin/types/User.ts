export type User = {
  id: number,
  name: string,
  email: string,
  password: string,
  created?: boolean,
  deleted?: boolean,
}

export type UserDetailed = {
  id: number,
  name: string,
  email: string,
  password: string,
  accessRights: []
}

export type UserForm = {
  name: string,
  email: string,
  password: string
}